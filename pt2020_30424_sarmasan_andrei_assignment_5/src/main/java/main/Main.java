package main;

import model.MonitoredData;
import service.MonitoredDataService;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        final MonitoredDataService monitoredDataService = MonitoredDataService.getMonitoredDataService();
        final List<MonitoredData> monitoredData = monitoredDataService.getMonitoredData();
        monitoredDataService.getDifferentDays(monitoredData);
        monitoredDataService.getFrequencyMap(monitoredData);
        monitoredDataService.getFrequencyMapByDay(monitoredData);
        monitoredDataService.getActivityAndDuration(monitoredData);
        monitoredDataService.getActivitiesWith90PercentRate(monitoredData);
    }

}
