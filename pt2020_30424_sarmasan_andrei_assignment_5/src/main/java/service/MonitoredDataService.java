package service;

import model.MonitoredData;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredDataService {

    private static MonitoredDataService monitoredDataService;

    public List<MonitoredData> getMonitoredData() {
        List<String> rows = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get("D:\\Programming\\Java\\Programming Techniques\\pt2020_30424_sarmasan_andrei_assignment_5\\Activities.txt"))) {
            rows = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        final List<MonitoredData> monitoredData = rows.stream().
                map(s -> s.split("\\t+")).
                map(data -> new MonitoredData(LocalDateTime.parse(data[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                        LocalDateTime.parse(data[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), data[2]))
                .collect(Collectors.toList());
        try {
            System.setOut(new PrintStream(new File("Task_1.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (final MonitoredData monitoredDataEntity : monitoredData) {
            System.out.println(monitoredDataEntity.getActivity() +
                    " : " + monitoredDataEntity.getStartTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) +
                    " -> " + monitoredDataEntity.getEndTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
        return monitoredData;
    }

    public int getDifferentDays(final List<MonitoredData> monitoredData) {
        try {
            System.setOut(new PrintStream(new File("Task_2.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int differentDays = (int) monitoredData.stream().map(data -> data.getStartTime().getDayOfYear()).distinct().count();
        System.out.println("Distinct days: " + differentDays);
        return differentDays;
    }

    public Map<String, Integer> getFrequencyMap(final List<MonitoredData> monitoredData) {
        try {
            System.setOut(new PrintStream(new File("Task_3.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Map<String, Integer> frequencyMap = monitoredData.stream().collect(Collectors.toMap(MonitoredData::getActivity, m -> 1, Integer::sum));
        for (final String activity : frequencyMap.keySet()) {
            System.out.println(activity + " has appeared " + frequencyMap.get(activity) + " times.");
        }
        return frequencyMap;
    }

    public Map<Integer, Map<Object, Long>> getFrequencyMapByDay(final List<MonitoredData> monitoredData) {
        try {
            System.setOut(new PrintStream(new File("Task_4.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Map<Integer, Map<Object, Long>> frequencyMap = monitoredData.stream().collect(
                Collectors.groupingBy(activity -> activity.getStartTime().getDayOfYear(), Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
        for (final Integer day : frequencyMap.keySet()) {
            System.out.println("Day " + day + ":" + frequencyMap.get(day));
        }
        return frequencyMap;
    }

    public Map<Object, List<Long>> getActivityAndDuration(final List<MonitoredData> monitoredData) {
        try {
            System.setOut(new PrintStream(new File("Task_5.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Map<Object, List<Long>> activityDuration = monitoredData.stream().
                collect((Collectors.groupingBy(MonitoredData::getActivity, Collectors.mapping(MonitoredData::seconds, Collectors.toList()))));
        activityDuration.forEach((m, t) -> {
            long time = t.stream().mapToLong(Long::longValue).sum();
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            System.out.println("Activity: " + m + " duration: " + simpleDateFormat.format(new Date(time * 1000L)));
        });
        return activityDuration;
    }

    public List<String> getActivitiesWith90PercentRate(final List<MonitoredData> monitoredData) {
        try {
            System.setOut(new PrintStream(new File("Task_6.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Map<String, Long> activityMap = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
        final Map<String, Long> timeMap = monitoredData.stream().filter(m -> m.seconds() / 60 < 5).collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
        final List<String> activities = monitoredData.stream()
                .filter(m -> timeMap.get(m.getActivity()) != null && timeMap.get(m.getActivity()) >= 0.9 * activityMap.get(m.getActivity()))
                .map(MonitoredData::getActivity).distinct().collect(Collectors.toList());
        System.out.println("There are " + activities.size() + " activities that have more than 90% of the monitoring records with duration less than 5 minutes");
        activities.forEach((m) -> System.out.println("Activity: " + m));
        return activities;
    }

    private MonitoredDataService() {
    }

    public static MonitoredDataService getMonitoredDataService() {
        if(monitoredDataService == null) {
            monitoredDataService = new MonitoredDataService();
        }
        return monitoredDataService;
    }
}
